import React from 'react';
import { makeStyles } from '@material-ui/core/styles';



const useStyles = makeStyles(theme => ({
  body:{
    background:'#1d1f20',
    width:'100%',
    height:'80vh'
  },
  ct:{
    height:'250px',
    width:'370px',
    border:'1px solid #f1c40f',
    margin:'100px auto',
    textAlign:'center',
    position:'relative',
    color:'#fff',
    padding:'15px',
  },
  span:{
    background:'#1d1f20',
    color:'#f1c40f',
    padding:'0 10px',
    fontSize:'20px',
    position:'relative',
    top:'-28px',
  },
  corner:{
    height:'30px',
    width:'30px',
    borderRadius:`50%`,
    border:'1px solid #fff',
    transform:`rotate(-45deg)`,
    position:'absolute',
    background:'#1d1f20',
    
  },
  left_top:{
    top:'-16px',
    left:'-16px',
    borderColor:'transparent transparent #f1c40f transparent' 
  },
  left_bottom:{
    bottom:'-16px',
    left:'-16px',
    borderColor:`transparent #f1c40f transparent transparent`,
  },
  right_top:{
    top:'-16px',
    right:'-16px',
    borderColor:'transparent transparent transparent #f1c40f'
  },
  right_bottom:{
    bottom:'-16px',
    right:'-16px',
    borderColor:'#f1c40f transparent transparent transparent'
  },
  p:{
    paddingTop:'13px',
    fontSize:'18px',
  },


}));
export default function  Quotes(){
  

  const classes = useStyles();

  return (
    <div className={classes.body}>
        <div className={classes.ct}>
          <div className={`${classes.corner} ${classes.left_top}`} ></div>
          <div className={`${classes.corner} ${classes.left_bottom}`} ></div>
          <div className={`${classes.corner} ${classes.right_top}`}></div>
          <div className={`${classes.corner} ${classes.right_bottom}`}></div>
          <span className={classes.span}>Winston Churchill</span>
          <blockquote>
            <p className={classes.p}><i>&ldquo;Success consists of going from failure to failure without loss of enthusiasm.&rdquo; </i></p>
          </blockquote>
        </div>
    </div>
    );
}