import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Fab from '@material-ui/core/Fab';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import {Devices,Payment} from '@material-ui/icons';
import ShoppingCart from '@material-ui/icons/ShoppingCart'
import { Badge } from '@material-ui/core';

const useStyles = makeStyles( theme=>({
  list: {
    width: 350,
  },
  Fab:{
    position:'absolute',
    right: theme.spacing(19),
    top:theme.spacing(16),
  },
}));

export default function CartPage(props) {
  const classes = useStyles();
  const [state, setState] = React.useState({
    right: false,
  });
  const toggleDrawer = (side, open) => event => {
    
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [side]: open });
  };

  const sideList = side => (
    <div
      className={classes.list}
      role="presentation"
      onClick={toggleDrawer(side, false)}
      onKeyDown={toggleDrawer(side, false)}
    >
      <List> {/* should i use froEach instead of map, so we will get same item multiple time into one secter also we can get total price */}
       <ListItemText secondary="Your Purchases:"/>
       {(props.quantity.length === 0 ) ? <span style={{padding:'8px', margin:'4px'}}>Nothing to appear yet...</span>: 
        props.quantity.map((p, index) => (
          <ListItem button key={index}>
              <ListItemIcon><Devices /></ListItemIcon>
            <ListItemText primary={p.product_name} secondary={p.price + " €"}/>
          </ListItem>
        ))}
      </List>
      <Divider />
      <List>
        
          <ListItem button>
            <ListItemIcon><Payment color="primary"/></ListItemIcon>
            <ListItemText secondary="Total Price" />
            <ListItemText primary={props.totalPrice+" €"} />
          </ListItem>
        
      </List>
    </div>
  );
    
  return (

    <div>
     
      <Fab variant="extended" className={classes.Fab} onClick={toggleDrawer('right', true)} color="secondary" >
          <Badge badgeContent={props.itemCounter} 
                  color="primary"
                  anchorOrigin={{
                    horizontal:'left',
                    vertical:'bottom'
                  }}>
                        <ShoppingCart/>Shpping List
          </Badge>
        </Fab>

      <Drawer anchor="right" open={state.right} onClose={toggleDrawer('right', false)}>
        {sideList('right')}
      </Drawer>
    </div>
  );
}