import React from 'react';
import {BrowserRouter as Router, Route, Link, Switch, Redirect} from 'react-router-dom';
import {makeStyles} from '@material-ui/core/styles';
import { AppBar, Tab, Tabs } from '@material-ui/core';
import HomePage from './Views/Home/Home';
import AboutPage from './Views/About/About';
import ProductPage from './Views/Products/ProductsPage';
/* import CartPage from './Views/Cart/Cart';
 */import ContactPage from './Views/Contact/Contact';
import {Home, Devices, ContactMail, Info} from '@material-ui/icons';
import Login from './Views/login/LoginIndex';

import {AuthProvider, useAuth} from './auth'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    },

  cart:{
      marginLeft:'40%'
    }
}));

const SecureRoute = props=>{
  const auth = useAuth()

  if(!auth.loggedIn){
    return <Redirect to ="/"/>
  }

  return <Route {...props}/>
}

export default function App() {

  const classes = useStyles();
  
  const [value, setValue] = React.useState(1);

  const handleChange = ( newValue) => {
    setValue(newValue);
  };
  
  return (
    <AuthProvider>
    <Router>
    

        <div className={classes.root}>
          <AppBar position="fixed">
              <Tabs value={value} onChange={handleChange} aria-label="abs">
                  <Tab value={value} icon={<Home/>} label="Home" component={Link} to="/home"/>
                  <Tab value={value} icon={<Devices/>} label="Products" component={Link} to="/products"/> 
                  <Tab value={value} icon={<ContactMail/>} label="Contact" component={Link} to="/contact"/>
                  <Tab value={value} icon={<Info/>} label="About" component={Link} to="/about"/>
            </Tabs>
              
          </AppBar>
       </div>  
      
       <Switch >
                        <Route exact path="/" component={Login}/>
                        <SecureRoute path="/home" component={HomePage}/>
                        <SecureRoute path="/products" component={ProductPage}/> 
                        <SecureRoute path="/contact" component={ContactPage}/>
                        <SecureRoute path="/about" component={AboutPage}/>
      </Switch> 
    </Router>
    </AuthProvider>
  );
}



