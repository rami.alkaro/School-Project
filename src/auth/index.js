import React from 'react'

export const AuthContext = React.createContext({
    login: () => {},
    logout: () => {},
    loggedIn: false,
    userName: "admin",
  })

export const useAuth = () => {
    return React.useContext(AuthContext)
}

export const AuthProvider = ({children}) => {
    const [loggedIn, setLoggedIn] = React.useState(false)
    const [userName, setUserName] = React.useState(false)
  
    const handleLogin = (userName) => {
      setLoggedIn(true)
      setUserName(userName)
    }
  
    const handleLogout = () => {
      setLoggedIn(false)
      setUserName("")
    }
  
    const contextValues = {
      login: handleLogin,
      logout: handleLogout,
      loggedIn,
      userName
    }
  
    return <AuthContext.Provider value={contextValues}>{children}</AuthContext.Provider>
  }
  